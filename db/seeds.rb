# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
def copy_avatar_fixture(user, file)
  fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', 'avatar', "#{file}.jpg")

  user.avatar.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end



james = User.create!(
  name: 'James Doe',
  email: 'jamesdoe@gmail.com',
  password: 'password',
  password_confirmation: 'password',
)
copy_avatar_fixture(james, :james)


janet = User.create!(
  name: 'Janet Doe',
  password: 'qwerty',
  password_confirmation: 'qwerty',
  email: 'janetdoe@gmail.com',
)
copy_avatar_fixture(janet, :janet)
