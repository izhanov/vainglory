Rails.application.routes.draw do
  devise_for :users

  resources :posts do
    resources :comments
  end

  post 'posts/:id' => 'posts#create', as: 'post_create'
  post 'posts/:post_id/comments' => 'comments#create', as: 'add_comment'

  put 'users/:user_id/:post_id/like' => 'likes#get_like', as: 'like'

  delete 'users/:user_id/:post_id/dislike' => 'likes#dislike', as: 'dislike'


  resources :users, only: [:show, :index] do
    member do
      get :following, :followers
    end
  end
  post 'users/:followed_id/follow' => 'relationships#create', as: 'follow'
  delete 'users/:followed_id/unfollow' => 'relationships#destroy', as: 'unfollow'


  root 'users#index'
end
