class Post < ApplicationRecord
  validates :caption, length: {maximum: 140}
  has_many_attached :photos

  belongs_to :user
  has_many :comments, dependent: :destroy

  has_many :likes

end
