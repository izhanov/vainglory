class PostsController < ApplicationController
  def index
    @posts = Post.all
  end

  def show
    find_post()
    @comment = @post.comments.build
    set_user()
  end

  def new
    @post = current_user.posts.build
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      redirect_to user_path(current_user.id)
    else
      render :new
    end

  end

  def destroy
    @post = Post.destroy(params[:id])
    redirect_to user_path(current_user.id)
  end

  private

  def post_params
    params.require(:post).permit(:caption, :user_id, photos: [])
  end

  def find_post
    @post = Post.find(params[:id])
  end

  def set_user
    find_post()
    @user = User.find(current_user.id)
  end
end
