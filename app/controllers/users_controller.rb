class UsersController < ApplicationController
  before_action :find_user, only: [:show, :update]
  before_action :require_login
  def index
    @users = User.all
  end

  def show
    @new_post = current_user.posts.build
  end

  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following
    render 'show_follow'
  end
  
  def followers
     @title = "Followers"
     @user  = User.find(params[:id])
     @users = @user.followers
     render 'show_follow'
   end

  private

  def find_user
    @user = User.find(params[:id])
  end

  def require_login
    unless current_user
      redirect_to new_user_session_path
    end
  end

end
