class CommentsController < ApplicationController
before_action :set_post

 def create
   @comment = @post.comments.build(comment_params)
   @comment.user_id = current_user.id

   if @comment.save
     redirect_to post_path(@post.id)
   else
     render :show
   end
 end

 def destroy
   @comment = @post.comments.find(params[:id]).destroy
 end
 private

 def comment_params
   params.require(:comment).permit(:content)
 end
 def set_post
   @post = Post.find(params[:post_id])
 end
end
