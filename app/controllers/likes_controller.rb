class LikesController < ApplicationController
  before_action :set_post

  def get_like
    @post.likes.create(user_id: current_user.id)
    redirect_to request.referrer
  end

  def dislike
    @post.likes.delete(@post.likes.where(user_id: current_user.id))
    redirect_to request.referrer
  end



  private

  def set_post
    @post = Post.find(params[:post_id])
  end

end
